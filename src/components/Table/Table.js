import React from "react";
import "./Table.scss";
import User from "../User/User";

const Table = ({
  tableHeadTitles,
  searchingData,
  fillteredData,
  users,
  searchDataHandler,
  sortBy,
  clearSearchValue,
  sortedData,
  sortingData,
  sortDirection
}) => {
  let data;
  if (searchingData) {
    console.log("stagod");
    data = fillteredData
      .slice(0, 20)
      .map((user) => <User user={user} key={user.id} />);
  } else if (sortingData) {
    data = sortedData
      .slice(0, 20)
      .map((user) => <User user={user} key={user.id} />);
  } else {
    data = users.slice(0, 20).map((user) => <User user={user} key={user.id} />);
  }

  let tableTitles = tableHeadTitles.map((thT) => {
    return (
      <th key={thT.id}>
        <input
          id={thT.id}
          value={thT.value}
          type="text"
          placeholder={`search ${thT.text.toLowerCase()}`}
          onChange={searchDataHandler}
          onFocus={clearSearchValue}
        />
        <button onClick={() => sortBy(thT.id)}>
          <h4>{thT.text}</h4>
          <span>
            <i className="asc">&#9660;</i>
            <i className='desc'>&#9660;</i>
          </span>
        </button>
      </th>
    );
  });

  return (
    <div className="table-wrapper">
      <table className="table">
        <thead>
          <tr>{tableTitles}</tr>
        </thead>
        <tbody>{data}</tbody>
      </table>
    </div>
  );
};

export default Table;
