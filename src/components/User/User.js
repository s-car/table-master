import React from "react";

const User = ({ user }) => {
  const { fullName, balance, isActive, registered, country, state } = user;
  
  return (
    <tr key={user.id}>
      <td>{fullName}</td>
      <td>{balance}</td>
      <td>{isActive.toString()}</td>
      <td>{registered}</td>
      <td>{state}</td>
      <td>{country}</td>
    </tr>
  );
};
export default User;
